#Bluemix Liberty Boilerplate#
I experienced problems deploying my own JSF/JSP based apps on Bluemix as something was missing. I decided to create a blank boilerplate for you to get started easily. The only thing you have to modify is the manifst.yml file to your Bluemix data.


```
#!java

applications:
- disk_quota: 1024M
  host: <yourappname>
  name: <yourappname>
  path: webStarterApp.war
  domain: <bluemix-dataspace>.mybluemix.net
  instances: 1
  memory: 512M
```